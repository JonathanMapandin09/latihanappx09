package mapandin.yonathan.latihan09

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context): SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {
    companion object{
        val DB_Name = "media"
        val DB_Ver = 1
    }


    override fun onCreate(db: SQLiteDatabase?) {
        val tVideo= "create table video(id_video text primary key, id_cover text not null, video_title text not null)"
        val insVideo= "insert into video(id_video,id_cover,video_title) values " +
                "('0x7f0c0003','0x7f06005f','Random 01')," +
                "('0x7f0c0004','0x7f060060','Random 02')," +
                "('0x7f0c0005','0x7f060061','Random 03')"
        db?.execSQL(tVideo)
        db?.execSQL(insVideo)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }
}